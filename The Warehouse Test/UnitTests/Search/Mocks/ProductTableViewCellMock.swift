//
//  ProductTableViewCellMock.swift
//  UnitTests
//
//  Created by Leonid Grebeniuk on 15.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation
@testable import The_Warehouse_Test

class ProductTableViewCellMock: ProductTableViewCellProtocol {

    var invokedConfigure = false
    var invokedConfigureCount = 0
    var invokedConfigureParameters: (searchResult: SearchResultProtocol, searchString: String)?
    var invokedConfigureParametersList = [(searchResult: SearchResultProtocol, searchString: String)]()

    func configure(searchResult: SearchResultProtocol, searchString: String) {
        invokedConfigure = true
        invokedConfigureCount += 1
        invokedConfigureParameters = (searchResult, searchString)
        invokedConfigureParametersList.append((searchResult, searchString))
    }
}
