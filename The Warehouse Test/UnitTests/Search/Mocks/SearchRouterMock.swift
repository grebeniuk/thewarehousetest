//
//  SearchRouterMock.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//


import UIKit
@testable import The_Warehouse_Test

class SearchRouterMock: SearchRouterProtocol {

    var invokedShowProduct = false
    var invokedShowProductCount = 0
    var invokedShowProductParameters: (product: ProductProtocol, Void)?
    var invokedShowProductParametersList = [(product: ProductProtocol, Void)]()

    func showProduct(product: ProductProtocol) {
        invokedShowProduct = true
        invokedShowProductCount += 1
        invokedShowProductParameters = (product, ())
        invokedShowProductParametersList.append((product, ()))
    }

    var invokedShowBarScanner = false
    var invokedShowBarScannerCount = 0

    func showBarScanner() {
        invokedShowBarScanner = true
        invokedShowBarScannerCount += 1
    }

    var invokedPrepare = false
    var invokedPrepareCount = 0
    var invokedPrepareParameters: (segue: UIStoryboardSegue, sender: Any?)?
    var invokedPrepareParametersList = [(segue: UIStoryboardSegue, sender: Any?)]()

    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        invokedPrepare = true
        invokedPrepareCount += 1
        invokedPrepareParameters = (segue, sender)
        invokedPrepareParametersList.append((segue, sender))
    }
}
