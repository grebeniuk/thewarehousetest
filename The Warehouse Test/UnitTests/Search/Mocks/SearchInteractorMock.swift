//
//  SearchInteractorMock.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation
@testable import The_Warehouse_Test

class SearchInteractorMock: SearchInteractorInputProtocol {

    var invokedPresenterSetter = false
    var invokedPresenterSetterCount = 0
    var invokedPresenter: SearchInteractorOutputProtocol?
    var invokedPresenterList = [SearchInteractorOutputProtocol?]()
    var invokedPresenterGetter = false
    var invokedPresenterGetterCount = 0
    var stubbedPresenter: SearchInteractorOutputProtocol!

    var presenter: SearchInteractorOutputProtocol? {
        set {
            invokedPresenterSetter = true
            invokedPresenterSetterCount += 1
            invokedPresenter = newValue
            invokedPresenterList.append(newValue)
        }
        get {
            invokedPresenterGetter = true
            invokedPresenterGetterCount += 1
            return stubbedPresenter
        }
    }

    var invokedSearchProducts = false
    var invokedSearchProductsCount = 0
    var invokedSearchProductsParameters: (searchString: String, Void)?
    var invokedSearchProductsParametersList = [(searchString: String, Void)]()

    func searchProducts(searchString: String) {
        invokedSearchProducts = true
        invokedSearchProductsCount += 1
        invokedSearchProductsParameters = (searchString, ())
        invokedSearchProductsParametersList.append((searchString, ()))
    }

    var invokedRequestNextPage = false
    var invokedRequestNextPageCount = 0

    func requestNextPage() {
        invokedRequestNextPage = true
        invokedRequestNextPageCount += 1
    }
}
