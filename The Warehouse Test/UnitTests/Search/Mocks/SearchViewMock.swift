//
//  SearchViewMock.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit
@testable import The_Warehouse_Test

class SearchViewMock: SearchViewProtocol {

    var invokedSetupTableView = false
    var invokedSetupTableViewCount = 0

    func setupTableView() {
        invokedSetupTableView = true
        invokedSetupTableViewCount += 1
    }

    var invokedSetupKeyboardNotification = false
    var invokedSetupKeyboardNotificationCount = 0

    func setupKeyboardNotification() {
        invokedSetupKeyboardNotification = true
        invokedSetupKeyboardNotificationCount += 1
    }

    var invokedSetupSearchController = false
    var invokedSetupSearchControllerCount = 0

    func setupSearchController() {
        invokedSetupSearchController = true
        invokedSetupSearchControllerCount += 1
    }

    var invokedSetupBarButton = false
    var invokedSetupBarButtonCount = 0

    func setupBarButton() {
        invokedSetupBarButton = true
        invokedSetupBarButtonCount += 1
    }

    var invokedReloadTableView = false
    var invokedReloadTableViewCount = 0

    func reloadTableView() {
        invokedReloadTableView = true
        invokedReloadTableViewCount += 1
    }

    var invokedReloadEmptyDataSet = false
    var invokedReloadEmptyDataSetCount = 0

    func reloadEmptyDataSet() {
        invokedReloadEmptyDataSet = true
        invokedReloadEmptyDataSetCount += 1
    }

    var invokedScrollViewToTop = false
    var invokedScrollViewToTopCount = 0

    func scrollViewToTop() {
        invokedScrollViewToTop = true
        invokedScrollViewToTopCount += 1
    }

    var invokedShowLoading = false
    var invokedShowLoadingCount = 0

    func showLoading() {
        invokedShowLoading = true
        invokedShowLoadingCount += 1
    }

    var invokedShowError = false
    var invokedShowErrorCount = 0
    var invokedShowErrorParameters: (error: PersistenceError, Void)?
    var invokedShowErrorParametersList = [(error: PersistenceError, Void)]()

    func showError(_ error: PersistenceError) {
        invokedShowError = true
        invokedShowErrorCount += 1
        invokedShowErrorParameters = (error, ())
        invokedShowErrorParametersList.append((error, ()))
    }

    var invokedDismissLoading = false
    var invokedDismissLoadingCount = 0

    func dismissLoading() {
        invokedDismissLoading = true
        invokedDismissLoadingCount += 1
    }
}
