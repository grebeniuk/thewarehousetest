//
//  SearchPresenterTests.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//


import XCTest
@testable import The_Warehouse_Test

class SearchPresenterTests: XCTestCase {

	var presenter: SearchPresenter!
	let view = SearchViewMock()
	let router = SearchRouterMock()
	let interactor = SearchInteractorMock()

	override func setUp() {
		super.setUp()
		presenter = SearchPresenter(view: view, interactor: interactor, router: router)
	}
    
    func testViewDidLoad() {
        presenter.viewDidLoad()
        XCTAssertTrue(view.invokedSetupTableView)
        XCTAssertTrue(view.invokedSetupKeyboardNotification)
        XCTAssertTrue(view.invokedSetupSearchController)
        XCTAssertTrue(view.invokedSetupBarButton)
    }
    
    func testSetVerticalOffset() {
        let testValue: CGFloat = 5.0
        presenter.setVerticalOffset(testValue)
        XCTAssertTrue(view.invokedReloadEmptyDataSet)
        XCTAssertEqual(presenter.verticalOffset, testValue)
    }
    
    func testNumberOfRows_whenEmptySearchResult() {
        let numberOfRows = presenter.numberOfRows()
        XCTAssertEqual(numberOfRows, 0)
    }
    
    func testNumberOfRows_whenNotEmptySearchResult() {
        presenter.searchResults = [SearchResult.makeStub()]
        let numberOfRows = presenter.numberOfRows()
        XCTAssertEqual(numberOfRows, 1)
    }
    
    func testConfigureProductTableViewCell() {
        let searchString = "Test"
        presenter.searchString = searchString
        presenter.searchResults = [SearchResult.makeStub()]
        let cell = ProductTableViewCellMock()
        presenter.configure(cell: cell, forRowAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell.invokedConfigure)
        XCTAssertEqual(cell.invokedConfigureParameters?.searchString, searchString)
    }

}
