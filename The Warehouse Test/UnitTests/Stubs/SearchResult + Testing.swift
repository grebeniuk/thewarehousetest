//
//  SearchResult + Testing.swift
//  UnitTests
//
//  Created by Leonid Grebeniuk on 15.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation
@testable import The_Warehouse_Test

extension SearchResult {
    
    static func makeStub() -> SearchResult {
        SearchResult(products: [Product.makeStub()], productDescription: "Test")
    }
    
}
