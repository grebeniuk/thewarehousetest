//
//  Product+Testing.swift
//  UnitTests
//
//  Created by Leonid Grebeniuk on 15.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation
@testable import The_Warehouse_Test

extension Product {

    static func makeStub() -> Product {
        return Product(barcode: "123", productDescription: "Test", imageURL: nil, price: nil)
    }

}
