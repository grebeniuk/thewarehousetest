//
//  ProductViewMock.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit
@testable import The_Warehouse_Test

class ProductViewMock: ProductViewProtocol {

    var invokedSetup = false
    var invokedSetupCount = 0
    var invokedSetupParameters: (product: ProductProtocol, Void)?
    var invokedSetupParametersList = [(product: ProductProtocol, Void)]()

    func setup(product: ProductProtocol) {
        invokedSetup = true
        invokedSetupCount += 1
        invokedSetupParameters = (product, ())
        invokedSetupParametersList.append((product, ()))
    }
}
