//
//  ProductInteractorMock.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation
@testable import The_Warehouse_Test

class ProductInteractorMock: ProductInteractorInputProtocol {

    var invokedPresenterSetter = false
    var invokedPresenterSetterCount = 0
    var invokedPresenter: ProductInteractorOutputProtocol?
    var invokedPresenterList = [ProductInteractorOutputProtocol?]()
    var invokedPresenterGetter = false
    var invokedPresenterGetterCount = 0
    var stubbedPresenter: ProductInteractorOutputProtocol!

    var presenter: ProductInteractorOutputProtocol? {
        set {
            invokedPresenterSetter = true
            invokedPresenterSetterCount += 1
            invokedPresenter = newValue
            invokedPresenterList.append(newValue)
        }
        get {
            invokedPresenterGetter = true
            invokedPresenterGetterCount += 1
            return stubbedPresenter
        }
    }

    var invokedGetPrice = false
    var invokedGetPriceCount = 0
    var invokedGetPriceParameters: (barcode: String, Void)?
    var invokedGetPriceParametersList = [(barcode: String, Void)]()

    func getPrice(barcode: String) {
        invokedGetPrice = true
        invokedGetPriceCount += 1
        invokedGetPriceParameters = (barcode, ())
        invokedGetPriceParametersList.append((barcode, ()))
    }
}
