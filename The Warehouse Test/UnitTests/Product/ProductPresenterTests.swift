//
//  ProductPresenterTests.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import XCTest
@testable import The_Warehouse_Test

class ProductPresenterTests: XCTestCase {

	var presenter: ProductPresenter!
	let view = ProductViewMock()
	let router = ProductRouterMock()
	let interactor = ProductInteractorMock()

	override func setUp() {
		super.setUp()
        presenter = ProductPresenter(view: view, interactor: interactor, router: router, product: Product.makeStub())
	}

}
