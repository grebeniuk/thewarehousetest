//
//  AppCoordinator.swift
//  The Warehouse Test
//
//  Created by Leonid Grebeniuk on 17.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit

@objc class AppCoordinator: NSObject {
    
    @objc class func start(window: UIWindow) {
        let viewController = StoryboardScene.Main.searchViewControllerID.instantiate()
        SearchConfigurator.configure(viewController: viewController)
        let rootViewController = UINavigationController(rootViewController: viewController)
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
}
