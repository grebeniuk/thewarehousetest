//
//  BarcodeScannerConfigurator.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import BarcodeScanner

class BarcodeScannerCodeDelegateConfigurator {

    static func configure(barcodeScanner: BarcodeScannerViewController) -> BarcodeScannerCodeDelegate {
		let interactor = BarcodeScannerInteractor()
        let router = BarcodeScannerRouter(viewController: barcodeScanner)
        let presenter = BarcodeScannerPresenter(interactor: interactor, router: router)
        barcodeScanner.codeDelegate = presenter
        return presenter
	}

}
