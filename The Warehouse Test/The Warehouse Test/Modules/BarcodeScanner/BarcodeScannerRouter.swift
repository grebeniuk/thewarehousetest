//
//  BarcodeScannerRouter.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit

class BarcodeScannerRouter: BarcodeScannerRouterProtocol {

	private weak var viewController: UIViewController?
    weak var barcodeScanner: UIViewController?

	init(viewController: UIViewController) {
		self.viewController = viewController
	}
    
    func showProduct(_ product: ProductProtocol) {
        let productViewController = StoryboardScene.Main.productViewControllerID.instantiate()
        ProductConfigurator.configure(viewController: productViewController, product: product)
        viewController?.present(productViewController, animated: true, completion: nil)
    }
    
    func showAlert(message: String, actionTitle: String, handler: @escaping (() -> Void)) {
        let alert = UIAlertController(title: "Oops!", message: message, preferredStyle: .alert)
        let defaultOkAction = UIAlertAction(title: actionTitle, style: .default) { _ in
            handler()
        }
        alert.addAction(defaultOkAction)
        viewController?.present(alert, animated: true, completion: nil)
    }

}
