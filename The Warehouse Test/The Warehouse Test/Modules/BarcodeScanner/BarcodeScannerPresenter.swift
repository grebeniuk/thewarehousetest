//
//  BarcodeScannerPresenter.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import BarcodeScanner

class BarcodeScannerPresenter: BarcodeScannerCodeDelegate {

	private let interactor: BarcodeScannerInteractorInputProtocol
    private let router: BarcodeScannerRouterProtocol

	init(interactor: BarcodeScannerInteractorInputProtocol,
         router: BarcodeScannerRouterProtocol) {
		self.interactor = interactor
        self.router = router
	}
    
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
      print(code)
        interactor.getPrice(barcode: code) { [weak self] (result) in
            if result.found == .yes,
               let product = result.product {
                self?.router.showProduct(product)
                controller.reset()
            } else {
                self?.router.showAlert(message: "We can't find this barcode in the system", actionTitle: "Scan another barcode") {
                    controller.reset()
                }
            }
        } failure: { [weak self] _ in
            self?.router.showAlert(message: "Something went wrong", actionTitle: "Try later") {
                controller.dismiss(animated: true, completion: nil)
            }
        }
    }

}
