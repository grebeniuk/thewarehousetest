//
//  BarcodeScannerInteractor.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

class BarcodeScannerInteractor: BarcodeScannerInteractorInputProtocol {
	
    let productService: ProductServiceProtocol
    
    init(productService: ProductServiceProtocol = ProductService()) {
        self.productService = productService
    }
    
    func getPrice(barcode: String,
                  success: @escaping GetPriceResponseSuccess,
                  failure: @escaping Failure) {
        productService.getPrice(barcode: barcode, success: success, failure: failure)
    }
}
