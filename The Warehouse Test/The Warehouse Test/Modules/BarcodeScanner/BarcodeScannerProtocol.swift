//
//  BarcodeScannerProtocol.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//


import UIKit

protocol BarcodeScannerPresenterProtocol: class {
    
    func showBarcodeScanner()
    
}

protocol BarcodeScannerInteractorInputProtocol: class {

    func getPrice(barcode: String,
                  success: @escaping GetPriceResponseSuccess,
                  failure: @escaping Failure)

}

protocol BarcodeScannerRouterProtocol: ViewRouter {
    func showProduct(_ product: ProductProtocol)
    func showAlert(message: String, actionTitle: String, handler: @escaping (() -> Void))
}
