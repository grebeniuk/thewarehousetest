//
//  SearchViewController.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class SearchViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    var presenter: SearchPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        presenter.router.prepare(for: segue, sender: sender)
    }
    
}

extension SearchViewController: SearchViewProtocol {
    
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.emptyDataSetSource = presenter
        tableView.emptyDataSetDelegate = presenter
        tableView.register(nibCell: ProductTableViewCell.self)
        tableView.register(nibCell: LoadingTableViewCell.self)
    }
    
    func setupKeyboardNotification() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardWillBeShown),
                                       name: UIResponder.keyboardWillShowNotification,
                                       object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillBeHidden),
                                       name: UIResponder.keyboardWillHideNotification,
                                       object: nil)
    }
    
    @objc private func keyboardWillBeShown(note: Notification) {
        let userInfo = note.userInfo
        guard let keyboardFrame = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
            return
        }
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardFrame.height, right: 0.0)
        tableView.contentInset = contentInset
        tableView.scrollIndicatorInsets = contentInset
        presenter?.setVerticalOffset(-keyboardFrame.height / 3)
    }
    
    @objc private func keyboardWillBeHidden(note: Notification) {
        let contentInset = UIEdgeInsets.zero
        tableView.contentInset = contentInset
        tableView.scrollIndicatorInsets = contentInset
        presenter?.setVerticalOffset(0)
    }
    
    func setupSearchController() {
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
        searchController.searchBar.delegate = self
    }
    
    func reloadTableView() {
        tableView.reloadData()
    }
    
    func setupBarButton() {
        if #available(iOS 13.0, *) {
            if let image = UIImage(systemName: "barcode.viewfinder") {
                let barScannerButton = UIBarButtonItem(image: image,
                                                       style: .plain,
                                                       target: self,
                                                       action: #selector(showBarScanner))
                navigationItem.setRightBarButton(barScannerButton, animated: false)
            }
        } else {
            // TODO: Fallback on earlier versions
        }
    }
    
    @objc private func showBarScanner() {
        presenter.showBarScanner()
    }
    
    func reloadEmptyDataSet() {
        tableView.reloadEmptyDataSet()
    }
    
    func scrollViewToTop() {
        tableView.setContentOffset(.zero, animated: false)
    }
    
}

extension SearchViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if presenter.numberOfResults() == indexPath.row {
            return tableView.dequeue(cell: LoadingTableViewCell.self, at: indexPath)
        } else {
            let cell = tableView.dequeue(cell: ProductTableViewCell.self, at: indexPath)
            presenter.configure(cell: cell, forRowAt: indexPath)
            return cell
        }
    }

}

extension SearchViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRowAt(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard cell is LoadingTableViewCell else { return }
        presenter.requestNextPage()
    }

}

extension SearchViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.searchProducts(searchString: searchText)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        presenter.resetSearch()
    }

}
