//
//  SearchConfigurator.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit

class SearchConfigurator {
    
    static func configure(viewController: SearchViewController) {
        let router = SearchRouter(viewController: viewController)
        let interactor = SearchInteractor()
        let presenter = SearchPresenter(view: viewController, interactor: interactor, router: router)
        interactor.presenter = presenter
        viewController.presenter = presenter
    }
    
}
