//
//  SearchProtocol.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

protocol SearchViewProtocol: BaseViewControllerProtocol {
    
    func setupTableView()
    func setupKeyboardNotification()
    func setupSearchController()
    func setupBarButton()
    func reloadTableView()
    func reloadEmptyDataSet()
    func scrollViewToTop()
    
}

protocol SearchPresenterProtocol: class, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    var router: SearchRouterProtocol { get }
    
    func viewDidLoad()
    func numberOfRows() -> Int
    func numberOfResults() -> Int
    func configure(cell: ProductTableViewCellProtocol, forRowAt indexPath: IndexPath)
    func didSelectRowAt(indexPath: IndexPath)
    func searchProducts(searchString: String)
    func resetSearch()
    func showBarScanner()
    func setVerticalOffset(_ verticalOffset: CGFloat)
    func requestNextPage()
    
}

protocol SearchInteractorOutputProtocol: class {
    
    func updateSearchResults(_ searchResults: [SearchResultProtocol], hitCount: Int)
    func showError(_ error: PersistenceError)
    func showNoResult(searchString: String)
    
}

protocol SearchInteractorInputProtocol: class {
    
    var presenter: SearchInteractorOutputProtocol? { get set }
    
    func searchProducts(searchString: String)
    func requestNextPage()
    
}

protocol SearchRouterProtocol: ViewRouter {
    
    func showProduct(product: ProductProtocol)
    func showBarScanner()
    
}
