//
//  SearchInteractor.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

class SearchInteractor: SearchInteractorInputProtocol {

    weak var presenter: SearchInteractorOutputProtocol?
    
    let productService: ProductServiceProtocol
    let debouncer: DebouncerProtocol
    
    var lastSearchString: String?
    var start: Int = 0
    let limit = 20
    
    init(productService: ProductServiceProtocol = ProductService(),
         debouncer: DebouncerProtocol = Debouncer(delay: 0.4)) {
        self.productService = productService
        self.debouncer = debouncer
    }
    
    func searchProducts(searchString: String) {
        start = 0
        searchProducts(searchString: searchString, limit: limit, start: start)
    }
    
    func requestNextPage() {
        if let lastSearchString = lastSearchString {
            start += limit
            searchProducts(searchString: lastSearchString, limit: limit, start: start)
        }
    }
    
    private func searchProducts(searchString: String, limit: Int, start: Int) {
        lastSearchString = searchString
        debouncer.run { [weak self] in
            guard !searchString.isEmpty else {
                return
            }
            self?.productService.searchProducts(searchString: searchString, limit: limit, start: start) { (response) in
                if searchString == self?.lastSearchString {
                    if response.found == .yes,
                       let results = response.results,
                       response.hitCount > 0 {
                        self?.presenter?.updateSearchResults(results, hitCount: response.hitCount)
                    } else {
                        self?.presenter?.showNoResult(searchString: searchString)
                    }
                }
            } failure: { error in
                self?.presenter?.showError(error)
            }
        }
    }
    
}
