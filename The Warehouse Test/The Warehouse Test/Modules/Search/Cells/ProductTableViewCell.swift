//
//  ProductTableViewCell.swift
//  The Warehouse Test
//
//  Created by Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit

protocol ProductTableViewCellProtocol {

    func configure(searchResult: SearchResultProtocol, searchString: String)

}

class ProductTableViewCell: UITableViewCell, ProductTableViewCellProtocol {

    func configure(searchResult: SearchResultProtocol, searchString: String) {
        let tokens = searchString.components(separatedBy: .whitespacesAndNewlines)
        let boldStrings = tokens.map { token in token.localizedLowercase }
        let searchString = searchResult.productDescription.localizedLowercase
        var boldRanges = [Range<String.Index>]()
        
        for boldString in boldStrings {
            
            var searchRange = searchString.startIndex ..< searchString.endIndex
            
            while true {
                guard let boldRange = searchString.range(of: boldString, options:
                    [], range: searchRange) else { break }
                boldRanges.append(boldRange)
                searchRange = boldRange.upperBound ..< searchString.endIndex
            }
            
        }
        
        let boldAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: textLabel?.font.pointSize ?? 17) ]
        let attributedString = NSMutableAttributedString(string: searchResult.productDescription)
        
        boldRanges.forEach { range in
            attributedString.setAttributes(boldAttributes, range: NSRange(range, in: searchString))
        }
        
        textLabel?.attributedText = attributedString
    }
    
}
