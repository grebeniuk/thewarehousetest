//
//  LoadingTableViewCell.swift
//  The Warehouse Test
//
//  Created by Leonid Grebeniuk on 15.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var indicator: UIActivityIndicatorView?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        indicator?.startAnimating()
    }
    
}
