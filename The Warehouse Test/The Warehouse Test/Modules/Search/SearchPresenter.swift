//
//  SearchPresenter.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class SearchPresenter: NSObject, SearchPresenterProtocol {
    
    private weak var view: SearchViewProtocol?
    private let interactor: SearchInteractorInputProtocol
    let router: SearchRouterProtocol
    
    var searchResults: [SearchResultProtocol]?
    
    var searchString: String?
    var error: PersistenceError?
    var verticalOffset: CGFloat = 0
    var hitCount = 0
    
    init(view: SearchViewProtocol,
         interactor: SearchInteractorInputProtocol,
         router: SearchRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.setupTableView()
        view?.setupKeyboardNotification()
        view?.setupSearchController()
        view?.setupBarButton()
    }
    
    func setVerticalOffset(_ verticalOffset: CGFloat) {
        self.verticalOffset = verticalOffset
        view?.reloadEmptyDataSet()
    }
    
    func numberOfRows() -> Int {
        let count = numberOfResults()
        if count > 0 && count < hitCount {
            return count + 1
        } else {
            return count
        }
    }
    
    func numberOfResults() -> Int {
        searchResults?.count ?? 0
    }
    
    func configure(cell: ProductTableViewCellProtocol, forRowAt indexPath: IndexPath) {
        guard let searchResult = getsSearchResult(forRowAt: indexPath),
              let searchString = searchString else {
            return
        }
        cell.configure(searchResult: searchResult, searchString: searchString)
    }
    
    func getsSearchResult(forRowAt indexPath: IndexPath) -> SearchResultProtocol? {
        guard let searchResults = searchResults,
              searchResults.count > indexPath.row else {
            return nil
        }
        return searchResults[indexPath.row]
    }
    
    func didSelectRowAt(indexPath: IndexPath) {
        guard let searchResult = getsSearchResult(forRowAt: indexPath),
              let product = searchResult.products.first else {
            return
        }
        router.showProduct(product: product)
    }
    
    func searchProducts(searchString: String) {
        searchResults = nil
        hitCount = 0
        self.searchString = searchString
        if searchString.isEmpty {
            view?.dismissLoading()
        } else {
            view?.showLoading()
        }
        view?.reloadEmptyDataSet()
        interactor.searchProducts(searchString: searchString)
    }
    
    func resetSearch() {
        view?.scrollViewToTop()
        DispatchQueue.main.async { [weak self] in
            self?.resetResults()
        }
    }
    
    func resetResults() {
        searchString = nil
        interactor.searchProducts(searchString: "")
        view?.dismissLoading()
        searchResults = nil
        hitCount = 0
        view?.reloadTableView()
    }
    
    func requestNextPage() {
        interactor.requestNextPage()
    }
    
    func showBarScanner() {
        router.showBarScanner()
    }
}

extension SearchPresenter: SearchInteractorOutputProtocol {
    
    func updateSearchResults(_ searchResults: [SearchResultProtocol], hitCount: Int) {
        error = nil
        self.hitCount = hitCount
        if self.searchResults == nil {
            self.searchResults = searchResults
        } else {
            self.searchResults?.append(contentsOf: searchResults)
        }
        view?.reloadTableView()
        view?.dismissLoading()
    }
    
    func showError(_ error: PersistenceError) {
        self.error = error
        searchResults = nil
        hitCount = 0
        view?.dismissLoading()
        view?.reloadTableView()
    }
    
    func showNoResult(searchString: String) {
        error = nil
        hitCount = 0
        searchResults = []
        view?.dismissLoading()
        view?.reloadTableView()
    }
    
}

extension SearchPresenter: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        if (searchString ?? "").isEmpty {
            return NSAttributedString(string: "Use search or barcode scanner to find products")
        } else if error != nil {
            return NSAttributedString(string: "Something went wrong")
        } else if searchResults == nil {
            return NSAttributedString(string: "")
        } else {
            return NSAttributedString(string: "We can't find this product in the system")
        }
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> NSAttributedString! {
        if (searchString ?? "").isEmpty {
            var attributes = [NSAttributedString.Key: AnyObject]()
            attributes[.foregroundColor] = UIView().tintColor ?? .blue
            let fullString = NSMutableAttributedString(string: "")
            if let barcodeImageString = NSAttributedString.getBarcodeImageString() {
                fullString.append(barcodeImageString)
                fullString.append(NSAttributedString(string: " "))
            }
            fullString.append(NSAttributedString(string: "Use Barcode Scanner", attributes: attributes))
            return fullString
        } else {
            return NSAttributedString(string: "")
        }
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        showBarScanner()
    }

    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return verticalOffset
    }

}
