//
//  SearchRouter.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import BarcodeScanner

class SearchRouter: SearchRouterProtocol {
    
    private weak var viewController: UIViewController?
    var barcodeScannerDelegate: BarcodeScannerCodeDelegate?
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
    
    func showProduct(product: ProductProtocol) {
        viewController?.perform(segue: StoryboardSegue.Main.productViewController, sender: product)
    }
    
    func showBarScanner() {
        let barcodeScanner = BarcodeScannerViewController()
        barcodeScannerDelegate = BarcodeScannerCodeDelegateConfigurator.configure(barcodeScanner: barcodeScanner)
        barcodeScanner.errorDelegate = self
        barcodeScanner.dismissalDelegate = self
        viewController?.present(barcodeScanner, animated: true, completion: nil)
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? ProductViewController,
            let product = sender as? Product {
            ProductConfigurator.configure(viewController: viewController, product: product)
        }
    }
    
}

extension SearchRouter: BarcodeScannerDismissalDelegate {
    
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
        barcodeScannerDelegate = nil
    }
    
}


extension SearchRouter: BarcodeScannerErrorDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        print(error)
        barcodeScannerDelegate = nil
    }
}
