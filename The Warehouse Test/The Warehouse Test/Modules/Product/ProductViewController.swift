//
//  ProductViewController.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var priceLabel: UILabel?
    @IBOutlet weak var productImage: UIImageView?
    
    var presenter: ProductPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }
    
}

extension ProductViewController: ProductViewProtocol {
    
    func setup(product: ProductProtocol) {
        if let productDescription = product.productDescription {
            nameLabel?.text = productDescription
        } else {
            nameLabel?.text = "Unknown product"
        }
        if let productImageUrl = product.imageURL {
            productImage?.downloaded(from: productImageUrl)
        }
        if let price = product.price?.price {
            priceLabel?.text = "$\(price)"
        } else {
            priceLabel?.text = ""
        }
    }
}
