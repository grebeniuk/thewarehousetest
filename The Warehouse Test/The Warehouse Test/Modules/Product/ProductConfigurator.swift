//
//  ProductConfigurator.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit

class ProductConfigurator {
    
    static func configure(viewController: ProductViewController, product: ProductProtocol) {
        let router = ProductRouter(viewController: viewController)
        let interactor = ProductInteractor()
        let presenter = ProductPresenter(view: viewController, interactor: interactor, router: router, product: product)
        interactor.presenter = presenter
        viewController.presenter = presenter
    }
    
}
