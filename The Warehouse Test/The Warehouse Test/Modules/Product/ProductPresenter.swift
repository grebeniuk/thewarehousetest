//
//  ProductPresenter.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit

class ProductPresenter: ProductPresenterProtocol {
    
    private weak var view: ProductViewProtocol?
    private let interactor: ProductInteractorInputProtocol
    let router: ProductRouterProtocol
    
    var product: ProductProtocol
    
    init(view: ProductViewProtocol,
         interactor: ProductInteractorInputProtocol,
         router: ProductRouterProtocol,
         product: ProductProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.product = product
    }
    
    func viewDidLoad() {
        view?.setup(product: product)
        guard product.price == nil else {
            return
        }
        interactor.getPrice(barcode: product.barcode)
    }
    
}

extension ProductPresenter: ProductInteractorOutputProtocol {
    
    func updateProduct(_ product: ProductProtocol) {
        self.product = product
        view?.setup(product: product)
    }
}
