//
//  ProductProtocol.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit

protocol ProductViewProtocol: class {
    
    func setup(product: ProductProtocol)
}

protocol ProductPresenterProtocol: class {
    
    var router: ProductRouterProtocol { get }
    
    func viewDidLoad()
    
}

protocol ProductInteractorOutputProtocol: class {
    
    func updateProduct(_ product: ProductProtocol)
    
}

protocol ProductInteractorInputProtocol: class {
    
    var presenter: ProductInteractorOutputProtocol? { get set }
    
    func getPrice(barcode: String)
    
}

protocol ProductRouterProtocol: ViewRouter {
    
}
