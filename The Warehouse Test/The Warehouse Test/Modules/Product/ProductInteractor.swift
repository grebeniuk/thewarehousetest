//
//  ProductInteractor.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

class ProductInteractor: ProductInteractorInputProtocol {
    
    weak var presenter: ProductInteractorOutputProtocol?
    
    let productService: ProductServiceProtocol
    
    init(productService: ProductServiceProtocol = ProductService()) {
        self.productService = productService
    }
    
    func getPrice(barcode: String) {
        productService.getPrice(barcode: barcode) { [weak self] (response) in
            guard let product = response.product else {
                // TODO: Handle no product
                return
            }
            self?.presenter?.updateProduct(product)
        } failure: { _ in
            // TODO: Handle error
        }
    }
}
