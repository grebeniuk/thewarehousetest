//
//  NetworkManager.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

protocol RequestBuilderProtocol {
    var url: URL? { get }
    var path: String { get }
    var httpMethod: String { get }
}

protocol NetworkManagerProtocol {
    func performRequest<T>(_ type: T.Type,
                           requestBuilder: RequestBuilderProtocol,
                           success: @escaping (_ result: T) -> Void,
                           failure: @escaping Failure) where T: Decodable
}

class NetworkManager: NetworkManagerProtocol {

    func performRequest<T>(_ type: T.Type,
                           requestBuilder: RequestBuilderProtocol,
                           success: @escaping (_ result: T) -> Void,
                           failure: @escaping Failure) where T: Decodable {
        guard let url = requestBuilder.url else { return }
        var request = URLRequest(url: url)
        request.httpMethod = requestBuilder.httpMethod
        request.addValue(Configuration.subscriptionKey, forHTTPHeaderField: "Ocp-Apim-Subscription-Key")
        
        URLSession.shared.dataTask(with: request) { data, _, error in
            if let error = error {
                print("error \(error.localizedDescription)")
                DispatchQueue.main.async {
                    failure(.general)
                }
            } else if let data = data {
                do {
                    let result = try JSONDecoder().decode(T.self, from: data)
                    DispatchQueue.main.async {
                        success(result)
                    }
                } catch {
                    DispatchQueue.main.async {
                        failure(.general)
                    }
                    print("error \(error.localizedDescription)")
                }
            }
            
        }.resume()
    }
}
