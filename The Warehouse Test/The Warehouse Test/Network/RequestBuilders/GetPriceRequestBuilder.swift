//
//  GetPriceRequestBuilder.swift
//  The Warehouse Test
//
//  Created by Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit

class GetPriceRequestBuilder: RequestBuilderProtocol {

    let barcode: String
    let path = "price.json"
    let httpMethod = "GET"
        
    var url: URL? {
        guard var urlComponents = URLComponents(string: Configuration.baseServiceURL + path),
              let deviceID = UIDevice.current.identifierForVendor?.uuidString else {
            return nil
        }
        urlComponents.queryItems = [
            URLQueryItem(name: "Barcode", value: barcode),
            URLQueryItem(name: "UserID", value: Configuration.userID),
            URLQueryItem(name: "MachineID", value: deviceID)
            
        ]
        urlComponents.percentEncodedQuery = urlComponents.percentEncodedQuery?.replacingOccurrences(of: "+",
                                                                                                    with: "%2B")
        return urlComponents.url
    }
    
    init(barcode: String) {
        self.barcode = barcode
    }
    
}
