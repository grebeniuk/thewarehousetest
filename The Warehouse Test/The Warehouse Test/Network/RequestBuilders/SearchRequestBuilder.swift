//
//  SearchRequestBuilder.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

class SearchRequestBuilder: RequestBuilderProtocol {

    let searchString: String
    let limit: Int
    let start: Int
    
    let path = "Search.json"
    let httpMethod = "GET"
        
    var url: URL? {
        guard var urlComponents = URLComponents(string: Configuration.baseServiceURL + path) else {
            return nil
        }
         let queryItems = [
            URLQueryItem(name: "Search", value: searchString),
            URLQueryItem(name: "UserID", value: Configuration.userID),
            URLQueryItem(name: "Start", value: "\(start)"),
            URLQueryItem(name: "Limit", value: "\(limit)")
        ]
        urlComponents.queryItems = queryItems
        urlComponents.percentEncodedQuery = urlComponents.percentEncodedQuery?.replacingOccurrences(of: "+",
                                                                                                    with: "%2B")
        return urlComponents.url
    }
    
    init(searchString: String, limit: Int, start: Int) {
        self.searchString = searchString
        self.limit = limit
        self.start = start
    }
    
}
