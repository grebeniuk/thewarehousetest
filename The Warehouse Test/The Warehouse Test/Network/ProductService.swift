//
//  ProductService.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

protocol ProductServiceProtocol: class {

    func searchProducts(searchString: String,
                        limit: Int,
                        start: Int,
                        success: @escaping SearchResponseSuccess,
                        failure: @escaping Failure)
    func getPrice(barcode: String,
                  success: @escaping GetPriceResponseSuccess,
                  failure: @escaping Failure)
    
}

class ProductService: ProductServiceProtocol {
    
    let networkManager: NetworkManagerProtocol

    init(networkManager: NetworkManagerProtocol = NetworkManager()) {
        self.networkManager = networkManager
    }
    
    func searchProducts(searchString: String,
                        limit: Int,
                        start: Int,
                        success: @escaping SearchResponseSuccess,
                        failure: @escaping Failure) {
        let requestBuilder = SearchRequestBuilder(searchString: searchString, limit: limit, start: start)
        networkManager.performRequest(SearchResponse.self,
                                      requestBuilder: requestBuilder,
                                      success: success,
                                      failure: failure)
    }
    
    func getPrice(barcode: String,
                  success: @escaping GetPriceResponseSuccess,
                  failure: @escaping Failure) {
        let requestBuilder = GetPriceRequestBuilder(barcode: barcode)
        networkManager.performRequest(GetPriceResponse.self,
                                      requestBuilder: requestBuilder,
                                      success: success,
                                      failure: failure)
    }
    
}
