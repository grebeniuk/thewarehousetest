//
//  UITableView.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit

// swiftlint:disable all
extension UITableView {
    func register<T: UITableViewCell>(nibCell: T.Type) {
        register(T.nib(), forCellReuseIdentifier: T.reuseIdentifier())
    }
    
    func dequeue<T: UITableViewCell>(cell: T.Type, at indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: T.reuseIdentifier(), for: indexPath) as! T
    }
}

