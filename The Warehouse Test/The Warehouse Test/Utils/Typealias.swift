//
//  Typealias.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

typealias Failure = (PersistenceError) -> Void
typealias Success = () -> Void
typealias SearchResponseSuccess = (SearchResponse) -> Void
typealias GetPriceResponseSuccess = (GetPriceResponse) -> Void
