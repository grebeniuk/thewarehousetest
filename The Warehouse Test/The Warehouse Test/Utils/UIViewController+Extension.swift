//
//  UIViewController.swift
//  The Warehouse Test
//
//  Created Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol BaseViewControllerProtocol: class {
    func showLoading()
    func showError(_ error: PersistenceError)
    func dismissLoading()
}

extension BaseViewControllerProtocol where Self: UIViewController {
    func showLoading() {
        SVProgressHUD.setContainerView(view)
        SVProgressHUD.show()
    }
    func dismissLoading() {
        SVProgressHUD.dismiss()
    }
    
    func showError(_ error: PersistenceError) {
        let title = "Error"
        var message = ""
        
        switch error {
        case .notConnectedToInternet:
            message = "No internet connection"
        case .general:
            message = "Something went wrong."
        }
        showAlert(title: title, message: message)
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultOkAction = UIAlertAction(title: "OK", style: .cancel)
        alert.addAction(defaultOkAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showError(error: Error) {
        self.showAlert(title: "Error", message: error.localizedDescription)
    }
}
