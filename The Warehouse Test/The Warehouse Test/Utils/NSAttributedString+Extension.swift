//
//  NSAttributedString+Extension.swift
//  The Warehouse Test
//
//  Created by Leonid Grebeniuk on 15.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import UIKit

extension NSAttributedString {
    
    static func getBarcodeImageString() -> NSAttributedString? {
        if #available(iOS 13.0, *),
           let barcodeImage = UIImage(systemName: "barcode.viewfinder") {
            let imageAttachment = NSTextAttachment()
            imageAttachment.image = barcodeImage.withTintColor(UIView().tintColor ?? .blue)
            let imageString = NSAttributedString(attachment: imageAttachment)
            return imageString
        } else {
            return nil
        }
    }
    
}
