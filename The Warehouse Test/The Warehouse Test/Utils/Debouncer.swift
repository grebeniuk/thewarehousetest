//
//  Debouncer.swift
//  The Warehouse Test
//
//  Created by Leonid Grebeniuk on 15.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

protocol DebouncerProtocol {
    
    func run(action: @escaping () -> Void)
    
}

class Debouncer: DebouncerProtocol {
    
    private let delay: TimeInterval
    private var workItem: DispatchWorkItem?
    
    init(delay: TimeInterval) {
        self.delay = delay
    }
    
    func run(action: @escaping () -> Void) {
        workItem?.cancel()
        workItem = DispatchWorkItem(block: action)
        DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: workItem!)
    }
    
}
