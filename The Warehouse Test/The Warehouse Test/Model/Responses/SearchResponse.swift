//
//  SearchResponse.swift
//  The Warehouse Test
//
//  Created by Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

struct SearchResponse: Codable {
    
    let results: [SearchResult]?
    let found: Found?
    let hitCountString: String?
    
    var hitCount: Int {
        Int(hitCountString ?? "0") ?? 0
    }
    
    enum CodingKeys: String, CodingKey {
        case results = "Results"
        case found = "Found"
        case hitCountString = "HitCount"
    }
    
}
