//
//  GetPriceResponse.swift
//  The Warehouse Test
//
//  Created by Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

struct GetPriceResponse: Codable {
    
    let product: Product?
    let found: Found?
    
    enum CodingKeys: String, CodingKey {
        case product = "Product"
        case found = "Found"
    }
    
}
