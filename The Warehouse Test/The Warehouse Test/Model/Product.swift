//
//  Product.swift
//  The Warehouse Test
//
//  Created by Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

protocol ProductProtocol {
    
    var barcode: String { get }
    var productDescription: String? { get }
    var imageURL: String? { get }
    var price: Price? { get }
    
}

struct Product: ProductProtocol, Codable {
    
    let barcode: String
    let productDescription: String?
    let imageURL: String?
    let price: Price?
    
    enum CodingKeys: String, CodingKey {
        case barcode = "Barcode"
        case productDescription = "Description"
        case imageURL = "ImageURL"
        case price = "Price"
    }
    
}
