//
//  Found.swift
//  The Warehouse Test
//
//  Created by Leonid Grebeniuk on 15.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

enum Found: String, Codable {
    case yes = "Y"
    case no = "N"
}
