//
//  Price.swift
//  The Warehouse Test
//
//  Created by Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

struct Price: Codable {
    
    let price: String
    
    enum CodingKeys: String, CodingKey {
        case price = "Price"
    }
    
}
