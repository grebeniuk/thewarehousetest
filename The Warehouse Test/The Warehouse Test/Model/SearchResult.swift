//
//  SearchResult.swift
//  The Warehouse Test
//
//  Created by Leonid Grebeniuk on 14.04.2021.
//  Copyright © 2021 The Warehouse Group Limited. All rights reserved.
//

import Foundation

protocol SearchResultProtocol {
    
    var products: [Product] { get }
    var productDescription: String { get }
    
}

struct SearchResult: Codable, SearchResultProtocol {
    
    let products: [Product]
    let productDescription: String
    
    enum CodingKeys: String, CodingKey {
        case products = "Products"
        case productDescription = "Description"
    }
    
}
